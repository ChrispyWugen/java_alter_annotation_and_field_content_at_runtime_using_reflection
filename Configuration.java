

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceClient;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * example configuration class contains external data from properties file
 * to alter existing annotations; class is NOT complete shown here
 */
public class Configuration {

	/**
	* method that handles adaption of different annotations or fields
	*/
    void adaptAnnotations(Configuration config) { //config object not neede if singleton

        //alter annotation WebServiceClient in class Service
        alterClassAnnotation(Service.class, WebServiceClient.class, getWebServiceClientAnnotationValue(config)); //config object not needed if singleton

        //alter QName attribute (field) in class Service
        alterServiceQName(config); //config object not needed if singleton


    }

	/**
	* method takes the annotation map of the given class and alters the given annotation
	*/

    private void alterClassAnnotation(Class serviceClass, Class<? extends Annotation> annotationToAlter, Annotation annotationValue) {

        try {
            Method method = Class.class.getDeclaredMethod("annotationData", null);
            method.setAccessible(true);
            Object annotationData = method.invoke(serviceClass);
            Field annotations = annotationData.getClass().getDeclaredField("annotations");
            annotations.setAccessible(true);
            Map<Class<? extends Annotation>, Annotation> map =
                    (Map<Class<? extends Annotation>, Annotation>) annotations.get(annotationData);
            map.put(annotationToAlter, annotationValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	* method alters field content of a specified class
	* in this case explicit class "Service" and field (attribute) "SERVICE_QNAME" 
	*/
    private void alterServiceQName(Configuration config) {
        Field field = null;
        try {
            field = Service.class.getDeclaredField("SERVICE_QNAME");
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        field.setAccessible(true);
        try {
            field.set(null, new QName(config.getNamespaceURI(), "service"));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

	/**
	* method creates a nested class of the annotation class (here WebServiceClient) and overrides 
	* inherited classes to get the new/altered values
	*/
    private Annotation getWebServiceClientAnnotationValue(Configuration config) { //better to use singelton object of config

        WebServiceClient oldAnnotation = (WebServiceClient) Service.class.getAnnotations()[0];
        Annotation annotationValue = new WebServiceClient() {

            @Override
            public String name() {
                return oldAnnotation.name();
            }

            @Override
            public String targetNamespace() {
                return config.getNamespaceURI();
            }

            @Override
            public String wsdlLocation() {
                return config.getWSDL_CPFR_Location();
            }

            @Override
            public Class<? extends Annotation> annotationType() {
                return oldAnnotation.annotationType();
            }
        };

        return annotationValue;
    }
	
}